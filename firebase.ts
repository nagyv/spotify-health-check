import firebase from "firebase/app"
import "firebase/analytics"
import "firebase/auth"
import "firebase/firestore"

import {firebase as firebaseConfig} from './config'

const App = firebase.apps.length ? firebase.app() : firebase.initializeApp(firebaseConfig)
const Analytics = typeof window !='undefined' ? firebase.analytics() : null
const DataBase = firebase.firestore()

const createHealthCheck = ({name}) => {
  return DataBase.collection("health-checks").add({
    name
    // deadline: firebase.firestore.Timestamp.fromDate(values.deadline.toDate())
  })
}

const getHealthCheck = (id) => {
  return DataBase.collection('health-checks').doc(id).get()
    .then(doc => {
      if(doc.exists) {
        return doc.data()
      }
      return null
    })
}

export default App
export {Analytics, createHealthCheck, getHealthCheck}