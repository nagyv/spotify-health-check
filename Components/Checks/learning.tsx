import { Button, Typography } from "@material-ui/core"
import React from "react"
import Image from 'next/image'
import { CheckContentWithParams } from "./CheckContent"
import { useStyles } from "./useStyles";


const Learning: CheckContentWithParams = ({currentTab, setTab}) => {
  const classes = useStyles()
  return <React.Fragment>
    <div className={classes.dimensionTitle}>
      <Image src={`/images/learning.jpg`} width={103} height={103*131/115} layout='intrinsic' />
      <Typography variant="h2">Learning</Typography>
    </div>
    <Typography paragraph>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
      ut labore et dolore magna aliqua. Rhoncus dolor purus non enim praesent elementum
      facilisis leo vel. Risus at ultrices mi tempus imperdiet. Semper risus in hendrerit
      gravida rutrum quisque non tellus. Convallis convallis tellus id interdum velit laoreet id
      donec ultrices. Odio morbi quis commodo odio aenean sed adipiscing. Amet nisl suscipit
      adipiscing bibendum est ultricies integer quis. Cursus euismod quis viverra nibh cras.
      Metus vulputate eu scelerisque felis imperdiet proin fermentum leo. Mauris commodo quis
      imperdiet massa tincidunt. Cras tincidunt lobortis feugiat vivamus at augue. At augue eget
      arcu dictum varius duis at consectetur lorem. Velit sed ullamcorper morbi tincidunt. Lorem
      donec massa sapien faucibus et molestie ac.
    </Typography>
    <Button onClick={() => setTab(currentTab + 1)}>Next</Button>
  </React.Fragment>
}

export default Learning