import { makeStyles, Theme } from "@material-ui/core";

export const useStyles = makeStyles((theme: Theme) => ({
  dimensionTitle: {
    display: 'flex',
    alignItems: 'center',
  },
}));
