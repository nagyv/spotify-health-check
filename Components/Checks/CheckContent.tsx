import React, { Dispatch, SetStateAction } from "react";

export type CheckContentWithParams = React.FC<{currentTab: number, setTab: Dispatch<SetStateAction<number>>}>;