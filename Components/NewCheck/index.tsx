import React, { useState } from 'react'
import { Formik, Form, Field } from 'formik';
import dayjs from 'dayjs'
import { TextField } from 'formik-material-ui';
import { Button, makeStyles, Snackbar, Typography } from '@material-ui/core';
import * as Yup from 'yup'
import { createHealthCheck } from '../../firebase';
import {CopyToClipboard} from 'react-copy-to-clipboard'
import Alert from '@material-ui/lab/Alert'
import {PageView} from '../../analytics'

// import { DatePicker } from 'formik-material-ui-pickers';

const HealthCheckSchema = Yup.object().shape({
  name: Yup.string().min(3).required()
})

const useStyles = makeStyles(
  {
    button: {
      marginTop: '1rem'
    }
  }
)

const NewCheck: React.FC = () => {
  const classes = useStyles()
  const [docRef, setRef] = useState('')
  const [copied, setCopied] = useState(false);

  const handleCloseCopied = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }

    setCopied(false);
  };
  // const [selectedDate, setSelectedDate] = useState(dayjs().add(7, 'day'));

  // const handleDateChange = (date) => {
  //   setSelectedDate(date);
  // };

  if(docRef) {
    return <div>
      <Typography variant="body1">Share with your team:</Typography>
      
      <CopyToClipboard text={`${window.location.href}health-checks/${docRef}`}
        onCopy={() => setCopied(true)}>
        <Typography variant="body1">
          {`${window.location.href}health-checks/${docRef}`}
        </Typography>
      </CopyToClipboard>
      
      <Snackbar open={copied} autoHideDuration={6000} onClose={handleCloseCopied}>
        <Alert onClose={handleCloseCopied} severity="success">
          URL copied to clipboard
        </Alert>
      </Snackbar>
        
    </div>
  }

  return <Formik
      initialValues={{
        name: '',
        // deadline: dayjs().add(7, 'day')
      }}
      validationSchema={HealthCheckSchema}
      onSubmit={(values, { setSubmitting }) => {
        PageView('New Health Check')
        createHealthCheck(values)
        .then(function(docRef) {
          setRef(docRef.id)
          setSubmitting(false);
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
        });
      }}>
      {({ submitForm, isSubmitting, isValid, dirty }) => (
        <Form>
          <Field
            component={TextField}
            name="name"
            type="text"
            label="Name of the health check"
            inputRef={input => input && input.focus()}
            fullWidth
          />
          {/* <Field 
            component={DatePicker} 
            name="date" 
            label="Feedback collection ends on" 
            fullWidth
            disableToolbar
            format="YYYY-MM-DD"
          /> */}
          <Button 
            className={classes.button}
            variant="contained" 
            fullWidth 
            color="primary"
            disabled={(isSubmitting || !isValid || !dirty)}
            onClick={submitForm}
            >Start</Button>
        </Form>
      )}
    </Formik>
}

export default NewCheck