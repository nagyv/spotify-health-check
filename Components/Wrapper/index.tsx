import { Container, createStyles, makeStyles, Theme } from "@material-ui/core";
import React from "react";
import Head from 'next/head'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    footer: {
      bottom: '0px',
      paddingBottom: theme.spacing(2),
    },
    main: {
      overflow: 'auto'
    }
  }),
);


const Wrapper: React.FC<{title?: string}> = ({children, title}) => {
  const classes = useStyles()
  return (
    <Container>
      <Head>
        <title>{ title ? `${title} | ` : ''}Spotify Health Checks</title>
      </Head>
      <div className={classes.main}>
        {children}
      </div>
      <footer className={classes.footer}>
        <hr />
        &copy; Viktor Nagy | Health check model: Henrik Kniberg & Kris1an Lindwall, with help from the other agile coaches at Spo1fy | Graphical design of cards: Mar1n Österberg
      </footer>
    </Container>
    )
}

export default Wrapper