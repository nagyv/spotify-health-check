import React from "react"
import { MuiPickersUtilsProvider } from "@material-ui/pickers"
import DayJSUtils from '@date-io/dayjs'
import { CssBaseline } from "@material-ui/core"
import { useRouter } from "next/router"
import { IntlProvider, useIntl } from "react-intl"
import * as locales from "../lang/compiled"
import { setLocale } from "yup"

const SetIntlLocales = () => {
  // const intl = useIntl()
  // setLocale({
  //   number: {
  //     min: intl.formatMessage(
  //       {
  //         description: 'Error message when longer input is required', // Description should be a string literal
  //         defaultMessage: 'Requires at least ${min} characters', // Message should be a string literal
  //       },
  //     )
  //   }
  // })
  return null
}

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  const { locale, defaultLocale, pathname } = router
  const messages = locales[locale]

  return <IntlProvider
    locale={locale}
    defaultLocale={defaultLocale}
    messages={messages}
  >
    <SetIntlLocales />
    <MuiPickersUtilsProvider utils={DayJSUtils}>
      <CssBaseline />
      <Component {...pageProps} />
    </MuiPickersUtilsProvider>
  </IntlProvider>
}

export default MyApp
