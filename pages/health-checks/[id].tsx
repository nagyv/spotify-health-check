import React from "react"
import { AppBar, Drawer, Hidden, IconButton, makeStyles, Tab, Tabs, Theme, Toolbar, Typography, useTheme } from "@material-ui/core"
import { PageView } from "../../analytics"
import { getHealthCheck } from "../../firebase"
import Wrapper from '../../Components/Wrapper'
import TabPanel from '../../Components/TabPanel'
import MenuIcon from '@material-ui/icons/Menu';
import Intro from "../../Components/Checks/intro"
import Support from "../../Components/Checks/support"
import Teamwork from "../../Components/Checks/teamwork"
import PawnsOrPlayers from "../../Components/Checks/pawns"
import Mission from "../../Components/Checks/mission"
import HealthOfCode from "../../Components/Checks/health"
import Process from "../../Components/Checks/process"
import DeliveringValue from "../../Components/Checks/delivering_value"
import Learning from "../../Components/Checks/learning"
import Speed from "../../Components/Checks/speed"
import Release from "../../Components/Checks/release"
import Fun from "../../Components/Checks/fun"
interface HealthCheckSchema {
  name: string
}

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    flexDirection: 'row-reverse',
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  title: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginRight: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(0),
  },
  dimensionTitle: {
    display: 'flex',
    alignItems: 'center',
  },
}));

function a11yProps(index: any) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const Dimensions = [
  {title: 'Intro',component: Intro},
  {title: 'Support',component: Support},
  {title: 'Teamwork',component: Teamwork},
  {title: 'Pawns or Players',component: PawnsOrPlayers},
  {title: 'Mission',component: Mission},
  {title: 'Health of Codebase',component: HealthOfCode},
  {title: 'Suitable Process',component: Process},
  {title: 'Delivering Value',component: DeliveringValue},
  {title: 'Learning',component: Learning},
  {title: 'Speed',component: Speed},
  {title: 'Easy Release',component: Release},
  {title: 'Fun',component: Fun},
]
  
const HealthCheck: React.FC<{locale: string, healthcheck: HealthCheckSchema}> = ({locale, healthcheck}) => {
  PageView('Health Check')
  const classes = useStyles()
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [tab, setTab] = React.useState(0);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const handleChange = (event: React.ChangeEvent<{}>, newtab: number) => {
    setTab(newtab);
    setMobileOpen(false);
  };

  const drawer = (
    <Tabs 
        orientation="vertical"
        variant="scrollable"
        value={tab}
        onChange={handleChange} 
        aria-label="Health Check dimensions">
        {Dimensions.map((item, index) => 
          <Tab key={`health-dimension-${index}`} label={item.title} {...a11yProps(index)} />
        )}
      </Tabs>
  )

  return <Wrapper title={healthcheck.name}>
    <Hidden smUp implementation='js'>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="h1" noWrap>Health Check: {healthcheck.name}</Typography>
        </Toolbar>
      </AppBar>
    </Hidden>
    <Hidden xsDown implementation="js">
      <Typography className={classes.title} variant="h1">Health Check: {healthcheck.name}</Typography>
    </Hidden>
      <div className={classes.root}>
        <nav className={classes.drawer} aria-label="mailbox folders">
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Hidden smUp implementation="js">
            <Drawer
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={mobileOpen}
              onClose={handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper,
              }}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="js">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              anchor='right'
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
        <main className={classes.content}>
          <Hidden smUp implementation='js'>
            <Toolbar />
          </Hidden>
          {Dimensions.map((item, index) => (
            <TabPanel key={`health-dimension-content-${index}`} value={tab} index={index}>
              {React.createElement(item.component, {
                currentTab: index,
                setTab
              })}
            </TabPanel>
          ))}
        </main>
      </div>
  </Wrapper>
}

export default HealthCheck

export async function getServerSideProps({locale, params}) {
  const HealthCheck = await getHealthCheck(params.id)

  if(!HealthCheck) {
    return {
      notFound: true
    }
  }

  return {
    props: {
      locale,
      healthcheck: HealthCheck
    }, // will be passed to the page component as props
  }
}