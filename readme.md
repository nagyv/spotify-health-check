# Spotify Health Check online

This application provides an interface to run and maintain [Spotify Health Checks](https://engineering.atspotify.com/2014/09/16/squad-health-check-model/)

For the health check content and designs: https://engineering.atspotify.com/wp-content/uploads/sites/2/2014/09/squad-health-check-model2.pdf
## Features / Plans

### Milestone 1

- No login required
- Create a squad by name, this provides a unique URL
- Share the URL with squad members
- Squad members can provide feedback a la Spotify Health Check 

### Milestone 2

- Authentication
- The squad's manager can start a new health check 
- The squad's manager can close a health check
- Health history view

### Milestone 3

- Organize squads into a "Company"
- Compare squad healths and their trends

## Tech stack

- NextJS
- Firebase Cloud Firestore - to start with

## Routes

### Milestone 1

Single team, single health-check works

- `/` - simple intro page to describe the service
- `/health-checks` - start new health check
- `/health-checks/:id` - a single health check instance
- `/health-checks/:id/status` - the current results of the health check

### Milestone 2

Single team with a history of health checks

- `/` - requires auth, redirects to `/teams/:id` if user has a team or to `/teams/new` otherwise
- `/auth/...` - signup, login, forgot password, oauth callback, logout, confirm e-mail
- `/teams/new`
- `/teams/:id`
- `/teams/:id/members`
- `/health-checks` - shows the health history for the user's team
- `/health-checks/new` - start new health check for the team

### Milestone 3

- `/` - should show your company and team stats
- `/teams` - lists the teams
