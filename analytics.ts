import {Analytics} from './firebase'

export function PageView(title: string) {
  if(typeof window =='undefined') return

  Analytics.logEvent('page_view', {
    page_title: title
  })
}